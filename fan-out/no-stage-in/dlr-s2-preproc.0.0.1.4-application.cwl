$graph:
- baseCommand: dlr-s2-preproc
  class: CommandLineTool
  id: node
  inputs:
    inp1:
      inputBinding:
        position: 1
        prefix: --input_reference
      type: string
    inp2:
      default: /workspace/data
      inputBinding:
        position: 2
        prefix: --data_path
      type: string
    inp3:
      default: 'Yes'
      inputBinding:
        position: 3
        prefix: --stage-in
      type: string
  outputs:
    results:
      outputBinding:
        glob: '*'
      type:
        items: File
        type: array
  requirements:
    EnvVarRequirement:
      envDef:
        PATH: /opt/anaconda/envs/env_app/bin:/opt/anaconda/condabin:/opt/anaconda/envs/env_app/bin:/opt/anaconda/bin:/home/jenkins/envs/bin:/opt/anaconda/condabin:/home/jenkins/workspace/t2pc/pipeline-conda@tmp/withMaven6f2615c8:/opt/anaconda/bin:/usr/local/bin:/bin:/usr/bin
  stderr: std.err
  stdout: std.out
- class: Workflow
  id: main
  inputs:
    data_path: string
    input_reference: string[]
    stage-in: string
  outputs:
  - id: outss
    outputSource:
    - step1/results
    type:
      items:
        items: File
        type: array
      type: array
  requirements:
  - class: ScatterFeatureRequirement
  steps:
    step1:
      in:
        inp1: input_reference
        inp2: data_path
        inp3: stage-in
      out:
      - results
      run: '#node'
      scatter: inp1
      scatterMethod: dotproduct
cwlVersion: v1.0
