class: Workflow
cwlVersion: v1.0
hints:
- class: SubworkflowFeatureRequirement
- class: DockerRequirement
  dockerPull: docker-co.terradue.com/ec-better/dlr-s2-preproc:0.0.1.4
- class: InitialWorkDirRequirement
  listing:
    - class: Directory
      location: /workspace/data
      path: data

id: dynamic
inputs:
  _data_path: string
  _input_reference: string[]
  _stage-in: string
outputs:
- id: dyout
  outputSource:
  - step1/outss
  type:
    items:
      items: 
        - File
        - Directory
      type: array
    type: array
steps:
  step1:
    in:
      data_path: _data_path
      input_reference: _input_reference
      stage-in: _stage-in
    out:
    - outss
    run: dlr-s2-preproc.0.0.1.4-application.cwl
